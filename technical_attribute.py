# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields

__all__ = ['TechnicalAttribute']


class TechnicalAttribute(ModelSQL, ModelView):
    'TechnicalAttribute'
    __name__ = 'party.technical.attribute'
    name = fields.Char('Name')
    is_default = fields.Boolean('Is Default')

    @classmethod
    def __setup__(cls):
        super(TechnicalAttribute, cls).__setup__()
