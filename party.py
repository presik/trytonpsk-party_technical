# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import PoolMeta, Pool
from trytond.model import ModelSQL, ModelView, fields
from trytond.transaction import Transaction
from trytond.wizard import Wizard, StateTransition

__all__ = ['Party', 'TechnicalLine', 'AddTechnicalAttributes']


class Party:
    __metaclass__ = PoolMeta
    __name__ = 'party.party'
    technical_lines = fields.One2Many('party.technical.line', 'party',
        'Technical Lines')

    @classmethod
    def __setup__(cls):
        super(Party, cls).__setup__()


class TechnicalLine(ModelSQL, ModelView):
    'Technical Line'
    __name__ = 'party.technical.line'
    party = fields.Many2One('party.party', 'Party', required=True)
    attribute = fields.Many2One('party.technical.attribute',
            'Technical Attribute', required=True)
    value = fields.Char('Value', required=False)
    comment = fields.Char('Comment')

    @classmethod
    def __setup__(cls):
        super(TechnicalLine, cls).__setup__()


class AddTechnicalAttributes(Wizard):
    'Add Technical Attributes'
    __name__ = 'party.technical.add_attributes'
    start_state = 'add_attributes'
    add_attributes = StateTransition()

    def transition_add_attributes(self):
        pool = Pool()
        Attribute = pool.get('party.technical.attribute')
        Party = pool.get('party.party')
        parties = Party.browse(Transaction().context['active_ids'])
        attributes = Attribute.search([
                ('is_default', '=', True),
                ])
        technical_lines = [{'attribute': a.id } for a in attributes]
        Party.write(parties, {
                'technical_lines': [('create', technical_lines)],
                })
        return 'end'
