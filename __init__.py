# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import Pool
from .party import (Party, TechnicalLine, AddTechnicalAttributes)
from .technical_attribute import TechnicalAttribute

def register():
    Pool.register(
        TechnicalAttribute,
        Party,
        TechnicalLine,
        module='party_technical', type_='model')
    Pool.register(
        AddTechnicalAttributes,
        module='party_technical', type_='wizard')
